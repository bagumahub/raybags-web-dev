## Hi. 
Raymond here, all about the internet of things.

1. ### Passionate About:
  - DevOps (aws - Jenkins)
  - Data engineering
  - Data Integration
  - Web development
  - Cloud computing
  - Bash scripting
  - etc...

### TECHNOLOGIES
  These are some of the technologies I work with:

| Languages              | Frontend      | Backend       | Database       | Data Visualization | (BaaS) && Bssd  | Testing     | Other           |
| :---:                  |  :---:        |  :---:        | :---:          | :---:            |  :---:          | :---:         | :---:         |
| JavaScript.            | ReactJs       | NodeJs        | MySQL         | CanvasJS          | Amazone (AWS)   |  Jest         | Linux         |
| Python                 | HTML          | ExpressJs     | MongoDB       |  Kibana.          | Kubernetes      |               | Git           |
| Groovy                 | CSS           | Hadoop        | PostgreSQL    | ChartJS.          | Bash            |               | Visual Studios |
| X-Query                | SCSS          | Kafka         | DynamoDb     |  RabbitMQ          | Firebase        |               | Postman        |
| jQuery.                | Bootstrap     |               | Cassandra                         |                 | Heroku        | | Jenkins        |     



### My Qualities:

 - [x]  Self taught
 - [x]  Highly Motivated
 - [x]  Self made
 - [ ]  Swimmer
